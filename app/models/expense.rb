class Expense < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  searchable do
     text :merchant
     text :comment
     text :category
     text :payment_mode
     date :date
     integer :total
     time :created_at 
  end     
end
