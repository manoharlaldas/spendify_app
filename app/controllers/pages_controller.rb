class PagesController < ApplicationController
  
  def home
    if signed_in?
      @title = "Home"
      @expense = Expense.new
      @expenses = current_user.expenses.paginate(:page => params[:page])  
    else
      redirect_to signin_path
    end
  end
 
  def contact
    @title = "Contact"
  end
  
  def about
    @title = "About"
  end
  
  def help
    @title = "Help"
  end
end

 

