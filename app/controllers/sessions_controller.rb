class SessionsController < ApplicationController
  # POST /sessions
  # POST /sessions.json
  def create
    user = User.authenticate(params[:session][:email],
                            params[:session][:password])
    if user.nil?
      flash.now[:error] = "Invalid email/password combination"
      render 'new'
    else
      sign_in user
      redirect_back_or user
    end
  end
  
  
  # DELETE /sessions/1
  # DELETE /sessions/1.json
  def destroy
    sign_out
    redirect_to root_path
  end
  # def user_email
  #   params[:user][:password]
  # end
  # def user_password
  #   parmas[:user][:password]
  # end
  # private
  #   # Use callbacks to share common setup or constraints between actions.
  #   def set_session
  #     @session = Session.find(params[:id])
  #   end

  #   # Never trust parameters from the scary internet, only allow the white list through.
  #   def session_params
  #     params[:session]
  #   end
end
