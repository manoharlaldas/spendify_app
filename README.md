![Screen Shot 2015-07-14 at 4.44.54 PM.png](https://bitbucket.org/repo/a4gBpe/images/2397510821-Screen%20Shot%202015-07-14%20at%204.44.54%20PM.png)



#README
         This is a simple app based on Ruby on Rails.
         
#APPLICATION

    This app will allow users to see there all expenses and to whom they have given it.It will help user to list there expenses by the name and comment.
#REQUIREMENT

    Ruby Version: ruby 2.2.1p85
    Rails Version: Rails 4.2.1
    Database: mysql Ver 14.14
    
#GEM
    gem paperclip
    gem simple_form
    gem sunspot_rails
    gem sunspot_solr
#INSTALLTION PROCESS FOR sunspot gem for FILRTERING AND SEARCHING
    gem 'sunspot_rails' 
    gem 'sunspot_solr
    bundle install 
    rails generate sunspot_rails:install 
    rake sunspot:solr:start
#INSTALLATION PROCESS FOR paperclip FOR IMAGE UPLOADING 
    Add below line in your (config/environments/development.rb ) file of your project:
        Paperclip.options[:command_path] = "/usr/local/bin/"
        
    IF you are using Mac OS X: brew install imagemagick
    If you are on ubuntu: sudo apt-get install imagemagick -y
    
#STATUS
    Under development
   
         
         


 




