require "book"
require "expense"
require "user"

fdescribe Book do
  it "fails to create an empty book" do
	  book = build(:book)
	  expect(book.save).to be_falsy
  end
end

fdescribe Expense do
  it "fails to create an empty expense" do
    expense = build(:expense)
    expect(expense.save).to be_falsy
  end
end

fdescribe User do
  it "fails to create an empty user" do
    user = build(:user)
    expect(user.save).to be_falsy
  end
end