# Test to check that if user have expense or not
require "user"
require "expense"

describe User do
  it "has no expense" do
	  expect(User.new).to be_empty
  end

  it "has expense when initialized with one" do
	  expect(User.new(expenses: [Expense.new])).not_to be_empty
  end

  it "count the number of expenses user have" do
	  expect(User.new(expenses: [Expense.new]).expenses_count).to eq 1
  end
end