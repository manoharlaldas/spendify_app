class User < ActiveRecord::Base
	# validates_presence_of :name

	def initialize expenses: []
		@expenses = expenses
	end

	def empty?
		@expenses.empty?
	end
    
    def expenses_count
    	@expenses.count 
    end
end